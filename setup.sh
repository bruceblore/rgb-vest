#!/bin/bash

echo "Updating system..."
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade

echo "Installing dependencies..."
sudo apt-get install python3-pip nodejs npm keyd
sudo pip install --break-system-packages adafruit-circuitpython-neopixel
sudo pip install --break-system-packages waitress
npm install --include=dev
npm run buildStart

echo "Placing files..."
sudo cp pixelController/pixelController.py /usr/bin/rgbvest-driver
sudo chown root:root /usr/bin/rgbvest-driver
sudo chmod 744 /usr/bin/rgbvest-driver

sudo mkdir /etc/rgb-vest
sudo cp defaultConfig.json /etc/rgb-vest/config.json

echo "Setting up systemd services..."
sudo cp pixelController/pixel-controller.service /etc/systemd/system/pixel-controller.service
sudo cp build/server/rgb-server.service /etc/systemd/user/rgb-server.service
sudo systemctl enable pixel-controller.service
systemctl --user enable rgb-server.service
loginctl enable-linger

echo "setting up keyd"
sudo systemctl enable --now keyd.service
sudo cp keyd/keyd.conf /etc/keyd/default.conf
sudo cp keyd/lightctl.py /usr/bin/lightctl

echo "Done! You may want to reboot now."

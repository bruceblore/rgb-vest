# Purpose

The files contained in this folder are related to the LED driver process. pixelController.py is the code for this process, and pixelController.service is a systemd service to start it on boot.

## Why a separate process?

Good reason: the rest of the code is fairly complex and involves interacting with other devices on a network. (Which *should* only allow access by trusted devices because it *should* be a phone hotspot, but it's best not to make assumptions.) Controlling the LEDs requires root. With a single process, we need to have a complex program with network access running as root, which is bad. (Probably not *that* bad because I wouldn't want to run anything important on an RGB vest, but again it's best not to make assumptions.) This allows us to have only a simple program of less than 200 lines running as root, which the more complex program with network access communicates with.

Bad reason: I couldn't find a RGB pixel driver library that I liked in Node. (They seemed to all require either complex configuration or an Arduino.) I don't like writing async code in Python. This is an easy way to slap the two together.

# API

A unix socket is created at a location defined in the config file. An http server is hosted on this unix socket. (A unix socket in particular was chosen to avoid overhead from the networking stack and as a low-effort way to prevent other machines from interacting directly with this process.) The main process controls this process by making an http request. The path represents an operation. At the moment, there are two:

    length: Returns the length of the string

    colors: sets the colors of the LEDs in accordance to a JSON string provided in the request body. If the JSON is invalid, take no action and return a 400 with "invalid json" in the response. If the JSON is valid but does not properly represent the correct amount of colors, take no action and return a 400 with "invalid colors" in the response. If the operation is successful, return 204 No Content.

Requesting any other path will return a 404. And yes, this is a write only API. I don't have a particularly great reason for it being a write-only API other than the fact that I have no use for a read function so I just didn't make one.
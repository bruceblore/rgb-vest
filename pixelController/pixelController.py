#!/usr/bin/env python3

try:
    import neopixel
    import board
except:
    from collections import UserList
    print('Failed to import neopixel and board. Are you on your dev machine?')
    print('Setting up neopixel emulation where all color changes are printed to the console')

    class neopixel:
        # Does literally nothing because we aren't using hardware, but provided for API compatibility
        GRB = 'GRB'
        GRBW = 'GRBW'
        RGB = 'RGB'
        RGBW = 'RGBW'

        class NeoPixel(UserList):
            # Initialize what we actually need for basic emulation
            def __init__(self, pin, n, *, bpp=3, brightness=1.0, auto_write=True, pixel_order=None):
                self.n = n
                self._autoWrite = auto_write
                self.data = [(0,) * bpp] * n
                self._shown = self.data
                if (self._autoWrite):
                    self.show()

            def write(self):
                self.show()

            def show(self):
                self._shown = self.data
                print(self.__repr__())
                print()

            # Do nothing because we haven't done anything to hardware that we need to undo
            def deinit(self):
                pass

            def fill(self, color):
                self.data = [color] * n

            def __repr__(self):
                return "[" + ", ".join([str(x) for x in self]) + "]"

            def __setitem__(self, index, newValue):
                self.data[index] = newValue
                if (self._autoWrite):
                    self.show()


import time
import traceback
from waitress import serve
import json

# Configuration
try:
    PIN = board.D18
except:
    PIN = 0
    pass

CONFIG_PATH = '/etc/rgb-vest/config.json'
PIXEL_ORDER = neopixel.RGB
with open(CONFIG_PATH) as file:
    config = json.JSONDecoder().decode(file.read())

# LED library initialization
pixels = neopixel.NeoPixel(PIN, config['leds']['stringLength'], bpp=len(
    PIXEL_ORDER), pixel_order=PIXEL_ORDER)

# LED library test
pixels[0:config['leds']['stringLength']] = [
    (255, 0, 0)] * config['leds']['stringLength']
time.sleep(0.3)
pixels[0:config['leds']['stringLength']] = [
    (0, 255, 0)] * config['leds']['stringLength']
time.sleep(0.3)
pixels[0:config['leds']['stringLength']] = [
    (0, 0, 255)] * config['leds']['stringLength']
time.sleep(0.3)
pixels[0:config['leds']['stringLength']] = [
    (255, 255, 255)] * config['leds']['stringLength']
time.sleep(0.3)


def colorValid(color):
    # A valid color may be a list or a tuple, assuming that the list will be converted to a tuple later. (The validation is done immediately upon getting the json)
    if type(color) is not list and type(color) is not tuple:
        return False

    if len(color) != 3:
        return False

    for subpixel in color:
        if type(subpixel) is not int:
            return False

    return True


def server(environ, startResponse):
    headers = [('Content-type', 'text/plain; charset=utf-8')]
    path = environ['PATH_INFO'].lstrip('/').split('/')[0]
    body = environ['wsgi.input'].read().decode()

    if path == 'length':
        startResponse('200 OK', headers)
        return [bytes(str(config['leds']['stringLength']), encoding='utf-8')]

    elif path == 'colors':
        try:
            colors = json.JSONDecoder().decode(body)
        except:
            startResponse('400 Bad Request', headers)
            print(body)
            return [bytes('invalid json', encoding='utf-8')]

        if (
            # If we were not given a list
            type(colors) is not list or
            # If we were given the wrong number of colors
            len(colors) != config['leds']['stringLength'] or
            # If any color is invalid
            len(list(filter(lambda color: not colorValid(color), colors))) != 0
        ):
            startResponse('400 Bad Request', headers)
            return [bytes('invalid colors', encoding='utf-8')]

        colors = [tuple(pixel) for pixel in colors]
        pixels[0:config['leds']['stringLength']] = colors
        startResponse('204 No Content', headers)
        return []
    else:
        startResponse('404 Not Found', headers)
        return [bytes('404 Not Found', encoding='utf-8')]


# Try to start the server, or throw an error if it fails
try:
    serve(server, unix_socket=config['socketPath'], unix_socket_perms='777')
except Exception as e:
    print('An error prevented the socket from being opened!')
    traceback.print_exception(e)
    pixels[1:config['leds']['stringLength']] = [
        (0, 0, 0)] * (config['leds']['stringLength'] - 1)
    while True:
        pixels[0] = (255, 0, 0)
        time.sleep(0.5)
        pixels[0] = (0, 0, 0)
        time.sleep(0.5)

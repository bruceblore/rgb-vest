# Proxy

This is a simple stunnel proxy setup to run on the phone to provide HTTPS access to the RGB vest using a localhost cert

## Reasoning

The Bangle only allows requests to https servers, but the RGB vest uses HTTP. Self signed TLS certs are a thing, but you can't just generate them for an IP address. (Well, you probably *could* with enough effort, but they aren't allowed and probably won't be honored anywhere.) You need to use a domain name, and apparently localhost counts. From the perspective of the Bangle making http requests proxied through Gadgetbridge, the phone is localhost. This means that the Bangle can connect to https services hosted on the phone, but still not on the vest. This proxy is used to provide that connection to the vest.

## Generating and trusting the cert
No certificates are distributed with this program, because it is a bad idea to trust random people on the internet issuing certificates. If you trust me in order to get the RGB vest to work, then I could theoretically MITM your HTTPS connections. Therefore, the certificates are generated locally. That way, you can only be MITMed by yourself.

To generate a certificate, install mkcert, clone the repo, cd into this directory, and run `./gencert.sh` on the PC. Then, trust `root.pem`. In order to make Gadgetbridge trust the cert, you need to install a Magisk module that copies user certs into the system cert store, such as [this one](https://github.com/NVISOsecurity/MagiskTrustUserCerts).

Remember to make sure that nobody else gets access to your `key.pem` file or your mkcert private key (probably located at ` ~/.local/share/mkcert/rootCA-key.pem`). If someone gets access to your `key.pem`, they could make their own `localhost` servers that would be trusted by your phone. This *shouldn't* be much of a problem, but protect it just in case. The mkcert private key is a bigger deal: with that, anyone could issue a certificate for *any* website and have it be trusted by your phone, defeating the purpose of using https.

## Running the proxy

cd into this directory, change the `connect =` line in `stunnel.conf` to point to the raspberry pi, and run `./proxy.sh` to launch stunnel.
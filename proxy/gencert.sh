#!/bin/bash

echo "Note: This needs to be run on a PC because mkcert does not seem to exist for termux. Press enter to confirm."
read

mkcert -cert-file cert.pem -key-file key.pem localhost
cp $(mkcert -CAROOT)/rootCA.pem root.pem

echo "Done! Trust root.pem, and protect key.pem."
# Hardware setup

1. Connect the first strip's data input to pin D18 on the raspberry pi. This is the sixth pin from the corner, on the side of the GPIO header closest to the edge.
    * If you are powering your LEDs with 5V, you are supposed to use a level shifter because the pi uses 3.3V levels. However, if you don't have one, you can probably get away with not using one. It might not work (though it did for me), but it shouldn't break anything. (I am not responsible if it does.)
    * If you are powering your LEDs directly from a lithium ion battery or any other source less than 4.7V (which 3.3V is 70% of), you should not need a level shifter.
2. Power the LEDs and the raspberry pi.
    * If using the same power supply for both, make sure that both get power from the supply directly. Do not power the LEDs through the pins on the Raspberry Pi. You will probably get away with using the 5V pin for one strip, and might even get away with using the 3.3V pin for one strip or the 5V pin for two strips if you're careful and lucky. But you risk crashes due to voltage drops and damage to the pi due to excessive current, so it's a bad idea.
    * If using a different power supply, the grounds must be connected to each other, and the LEDs must be powered before the pi. If the pi is powered first, the LEDs might attempt to draw power through the data pin, which will probably damage both the LEDs and the pi.

# Software Setup

1. Make a fresh ISO of the minimal Rapberry Pi OS image. You may be able to get away with using an old one, but a fresh one is more likely to work
2. On the boot partition, create a file named `ssh` and leave it blank
3. On the boot partiton, create a file named `wpa_supplicant.conf` and fill it with the following to connect to your phone's hotspot:
    ```
    country=YOUR_COUNTRY_CODE
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1

    network={
        ssid="YOUR_HOTSPOT_NAME"
        psk="YOUR_HOTSPOT_PASSWORD"
    }
    ```
    Don't worry, we only downloading a little bit of data now, and then probably never downloading anything again.
4. Choose a username and password for your pi. Run the password through the command `echo 'password' | openssl passwd -6 -stdin`. On the boot partition, create a file named `userconf.txt`. The contents of this file should be your username, a colon, and then the generated password. For example, if you want the username pi and the password raspberry like what was the default before this requirement was introduced, use `pi:$6$Eg2iNLEZBvyGzNxY$9SrERCUudQ5alAVAiO1r5LL8r/cGvMfqkuLMQVUCUxNHWNHjrefIGXwpXvt9p.s8S0aYeHzI4Hr0LQEky4N.10`. (Though I don't recommend just using the default username and password.)
5. Change directory to the repo if you aren't there and run `npm install --dev; npm run build` to compile the server code from Typescript to Javascript.
5. Copy the contents of the repo into `/home/pi/rgb/` in the rootfs partition of the SD card. If there is not enough room, boot the pi, shut it down, and try again. (It will increase the data partition up to the size of your SD card.)
6. Start your hotspot, turn on the pi, wait for it to connect, and ssh into it either from your current machine or directly from your phone. The command to ssh in is `ssh <username>@<IP address>`. You should be able to find the IP address in your phone's hotspot settings, but you also may (or may not) be able to use `raspberrypi` or `raspberrypi.local` in place of it. If your phone doesn't display the IP addresses of connected devices, run `ip neigh` in the terminal on your phone and try the IP addresses listed there.
7. Run the following commands to install the dependencies and set up the code to run on boot:
    ```
    cd rgb
    ./setup.sh
    ```
7. If you want to edit the configuration, edit `/etc/rgb-vest/config.json`
8. Run `sudo reboot` to reboot the pi. It should be running the software after the reboot.

# Configuration

Because I have not yet found a clean way to support choosing the pin and pixel order, those values are hardcoded in pixelController/pixelController.py, which gets copied to /usr/bin/rgbvest-driver. Look for the "Configuration" comment to find them. All other global configurable values are present in the config file. A default config file is included here at defaultConfig.json, and it gets copied to /etc/rgb-vest/config.json during setup, which is where it is read from during execution. The available options are:

    socketPath: The path to the Unix socket used for communication between the LED driver process and the server process. There probably isn't a good reason to change this.

    leds: A category representing attributes of the LED string
        stringLength: The total number of LEDs present

        regions: The LED string can be divided into regions based on where different sections of the string are physically located. The contents consist of zero or more key/value pairs, where the key is a name and the value is an array of 2 numbers, with the first being the index of the first LED included, and the last being the index of the last LED included + 1. Some of the provided effects expect frontLeft, backLeft, backRight, and frontRight to be defined, so they should not be deleted. However, additional regions can be defined if you wish to define custom effects that rely on them, and overlap is allowed if you're careful about it.

    interpolator: A category representing settings that should apply to all interpolators
        resolution: The maximum number of states per second of transition time

    onStart: A category representing settings that will be applied on start
        colorScheme: The ID of the color scheme
        effect: The ID of the effect
        interpolator: The ID of the interpolator
        disabledColors: A list of color IDs from the color scheme to disable
        effectOptions: Options to override the options for the chosen effect
        interpolatorOptions: Options to override the options for the chosen interpolator
        general: General options
            ledPower: Whether the LEDs are lit
            brightness: A number from 0 to 1 representing what to unconditionally multiply every value by before applying any other operations
            maxChannel: If a subpixel value exceeds this number, temproarily bump down the brightness until it doesn't. Useful for preserving LED life or limiting brightness.
            maxAverage: If the average of all subpixels exceeds this number, temproarily bump down the brightness until it doesn't. Useful for limiting brightness and power consumption when many LEDs are lit while allowing more brightness when few are lit.
            redScale, greenScale, blueScale: Scale the brightness of individual subpixels. Useful for calibration

    network: Network-related options
        port: The port that the webpage and API will be served on

# Code overview

The code is divided mainly into several folders, though there are a few files left outside. More details about the contents of the folders will be present in README.md files inside the folders, with the exception of `node_modules`. The files and folders are:

* `./bangle-http-test`: A simple HTTP hello world server for testing purposes
* `./node_modules/`: A folder that has to exist because I used npm to install node types. It really should at least be in `./server`, but it doesn't work there when I have the entire project open.
* `./pixelController/`: The RGB LED driver process. Runs as root and allows a non-root process such as the main server to push states to the LEDs over an HTTP API on a Unix socket.
* `./proxy/`: A proxy to allow the Bangle to connect to the vest with HTTPS
* `./server/`: The main code, providing an API to manage color schemes and effects.
* `./defaultConfig.json`: The default "global" configuration that applies across all effects.
* `./package-lock.json`: A file that has to exist because I used npm to install node types. It really should at least be in `./server`, but it doesn't work there when I have the entire project open.
* `./package.json`: A file that has to exist because I used npm to install node types. It really should at least be in `./server`, but it doesn't work there when I have the entire project open.
* `./setup.sh`: A quick script to place the files in the correct place on a raspberry pi to run seemlessly on boot

The Bangle app is located in my [BangleApps](https://gitlab.com/bruceblore/BangleApps) repo, with the id [rgbcontrol](https://gitlab.com/bruceblore/BangleApps/-/tree/master/apps/rgbcontrol).

# Power on
Boot the pi. When the LED driver process (pixelController.py) starts, the LEDs should cycle red, green, blue, then white in that order. If the lights never turn on, the process either never started, or is failing to control the LEDs. This could be a bad connection, the wrong pin being set, or system configuration issues preventing the GPIO pins from being controlled. If the LEDs cycle colors, but not in that order, it is likely that the pixel order is set incorrectly.

Next, the LEDs should go off. If they stay white, that means that the controller process has crashed. If the first LED begins flashing red, that means that the LED driver process failed to create the socket for the main server to connect to.

The pi should eventually connect to your hotspot. When this happens, you can start sending API commands to `<raspberrypi's IP address>:8080`. I have provided the code for the banglejs GUI that I personally use. If I feel like doing it, I might write a web GUI at some point too. If you wish to write your own client, the API is described in server/API.md.

## Power off
There are two ways to do this:
1. Select the "Power down" effect from the effects menu and check the box indicating that you are sure you want to.
2. ssh into the pi and run `sudo systemctl poweroff`.

Once the green LED on the pi flashes 10 times, you can safely power off. If you are using separate power supplies, disconnect the pi first.

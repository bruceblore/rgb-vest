# File loading

All files in this folder with a `.js` or `.ts` file extension, but NOT with a `.d.ts` file extension, will be attempted to be loaded as interpolators, except for `interpolator.ts`. If you do not wish to load a file, you might wish to give it another extension, like `.ts.disabled`. The part of the file before the `.js` or `.ts` will be the ID of the color scheme.

`interpolator.ts` is just a valid but empty interpolator that other interpolators should inherit from.

# File format

The interpolator files should be Javascript modules which can be `require()`ed by file name. They export a class called Interpolator. Detailed descriptions of what is available to be defined will be in `interpolator.ts`.
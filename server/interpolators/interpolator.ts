export default class {
    static friendlyName = 'Base interpolator';
    static description = 'What all interpolators inherit from';

    options: Options = {};
    config: Config;
    setLEDState: Function;
    stateQueue: Array<Array<Color>>;    // Element zero is current state, final element is target state. When changing state, dequeue item zero THEN set state to new item zero.
    timeouts: Array<ReturnType<typeof setTimeout>> = [];
    lastTimeoutFinish: number = 0;

    /**
     * Initialize the interpolator
     *
     * @param config: The global configuration
     * @param setLEDState: The function that will be called to set the state of the LEDs. Should accept an array of n elements, where n is the number of LEDs on the string, each of which is an array of three numbers from 0 to 255 representing RGB values.
     */
    constructor(config: Config, setLEDState: Function, initialColors: Array<Color>) {
        this.config = config;
        this.setLEDState = setLEDState;
        this.stateQueue = [initialColors];
        this.setLEDState(initialColors);
    }

    /**
     * Skip all of the requested transitions and go to the final requested colors
     */
    snapToEnd() {
        this.clearTimeouts();
        this.stateQueue = [this.stateQueue[this.stateQueue.length - 1]];
        this.setLEDState(this.stateQueue[0]);
    }

    /**
     * Return the current color
     */
    getCurrentColors(): Array<Color> {
        return this.stateQueue[0];
    }

    /**
     * Skip all of the transitions and go to the colors passed as a parameter
     *
     * @param nextColors: the colors that are being snapped to
     */
    snapToNextColors(nextColors: Array<Color>) {
        this.clearTimeouts();
        this.stateQueue = [nextColors];
        this.setLEDState(this.stateQueue[0]);
    }

    /**
     * Queue the transition to the next color. This is likely the only thing that you will want to modify, as it is what affects what the transition will look like.
     *
     * A maximum transition time may be optionally specified. If it is specified, then a check should be done to see if it would take too long for the requested state to appear. If so, snapToNextColors should be called instead.
     *
     * Note: No code is provided to do the time check, as this class does not know how long the transition effect you want to do will take. To time your transitions, add your desired state to the end of stateQueue and use this.callAtEnd(this.popStateQueue, number), where number is how long between the previous state and this one.
     *
     * @param nextColors: the target state
     * @param maxTime: Maximum time before calling snapToNextColors instead
     */
    setNextColors(nextColors: Array<Color>, maxTime?: number) {
        this.stateQueue.push(nextColors);

        // Wrapping popStateQueue in an arrow function is necessary because otherwise the context gets lost and this doesn't work inside of it.
        this.callAtEnd(() => { this.popStateQueue() }, 0);
    }

    /**
     * @return the current options
     */
    getOptions(): Options {
        return this.options;
    }

    /**
     * Set options and respond appropriately
     *
     * @param options: The new options that will be set
     */
    setOptions(options: Options) {
        this.snapToEnd();
        this.options = options;
    }

    /**
     * Delete all of the timeouts
     */
    protected clearTimeouts() {
        for (let timeout of this.timeouts) {
            clearTimeout(timeout);
        }
        this.timeouts = [];
        this.lastTimeoutFinish = 0;
    }

    /**
     * Like setTimout, except the time is relative to the end of the queue. If we are at the end of the queue, THEN it is relative to the present.
     *
     * @param func: The function that should be called
     * @param timeout: How many milliseconds after the end of the queue the function should be called
     */
    protected callAtEnd(func: Function, timeout: number) {
        let time: number;

        if (this.stateQueue.length == 1) {
            time = timeout;
            this.lastTimeoutFinish = time;
        } else {
            time = this.lastTimeoutFinish + timeout;
            this.lastTimeoutFinish += timeout;
        }

        this.timeouts.push(setTimeout(() => { func() }, time));
    }

    /**
     * If the length of the state queue is 1, do nothing. Otherwise, pop item 0 off of the state queue, set the state to item zero, and pop the timeout from the list of timeouts
     */
    protected popStateQueue() {
        if (this.stateQueue.length == 1) {
            console.log('WARNING: popStateQueue called when stateQueue length is 1! Possible bug in interpolator.');
        } else {
            this.timeouts.shift();
            this.stateQueue.shift();
            this.setLEDState(this.stateQueue[0]);
        }
    }
}
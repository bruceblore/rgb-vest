import Interpolator from './interpolator';

type InterpolatorOptions = {
    fadeTime: NumberOption
    // maxQueueLen: NumberOption
}

export default class extends Interpolator {
    static friendlyName = 'Linear';
    static description = 'Linear fade between colors';

    options: InterpolatorOptions = {
        fadeTime: {
            name: 'Fade time',
            detail: 'How long between adjacent colors',
            type: "number",
            value: 0.1,
            min: 0,
            step: 0.01
        },
        // maxQueueLen: {
        //     name: 'Max queue length',
        //     detail: 'How long the color queue can get before we start dropping transitions',
        //     type: 'number',
        //     value: 50,
        //     min: 1,
        //     step: 1
        // }
    };
    // requestedColorQueue: Array<Array<Color>> = [];

    /**
     * Initialize the interpolator
     *
     * @param config: The global configuration
     * @param setLEDState: The function that will be called to set the state of the LEDs. Should accept an array of n elements, where n is the number of LEDs on the string, each of which is an array of three numbers from 0 to 255 representing RGB values.
     */
    constructor(config: Config, setLEDState: Function, initialColors: Array<Color>) {
        super(config, setLEDState, initialColors);
    }

    /**
     * Queue the transition to the next color. This is likely the only thing that you will want to modify, as it is what affects what the transition will look like.
     *
     * A maximum transition time may be optionally specified. If it is specified, then a check should be done to see if it would take too long for the requested state to appear. If so, snapToNextColors should be called instead.
     *
     * Note: No code is provided to do the time check, as this class does not know how long the transition effect you want to do will take. To time your transitions, add your desired state to the end of stateQueue and use this.callAtEnd(this.popStateQueue, number), where number is how long between the previous state and this one.
     *
     * @param nextColors: the target state
     * @param maxTime: Maximum time before calling snapToNextColors instead
     */
    setNextColors(nextColors: Array<Color>, maxTime?: number) {
        this.snapToEnd();

        let lastColors: Array<Color> = this.stateQueue[this.stateQueue.length - 1];
        let transitionTime: number = 1000 * this.options.fadeTime.value;
        let nTransitionStates = Math.ceil(this.options.fadeTime.value * this.config.interpolator.resolution)
        let timePerState = transitionTime / nTransitionStates

        for (let i = 1; i <= nTransitionStates; i++) {
            let intState: Array<Color> = [];
            for (let j = 0; j < this.config.leds.stringLength; j++) {
                let oldColor = lastColors[j];
                let targetColor = nextColors[j];

                intState.push([
                    oldColor[0] + ((targetColor[0] - oldColor[0]) * i / nTransitionStates),
                    oldColor[1] + ((targetColor[1] - oldColor[1]) * i / nTransitionStates),
                    oldColor[2] + ((targetColor[2] - oldColor[2]) * i / nTransitionStates),
                ])
            }

            this.stateQueue.push(intState);
            // Wrapping popStateQueue in an arrow function is necessary because otherwise the context gets lost and this doesn't work inside of it.
            this.callAtEnd(() => { this.popStateQueue() }, timePerState);
        }
    }
}
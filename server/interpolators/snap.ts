import Interpolator from './interpolator';

export default class extends Interpolator {
    static friendlyName = 'Snap';
    static description = 'Instantly snap, no transition effect';

    // This is just the base interpolator with a real name
}
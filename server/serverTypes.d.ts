// Represents the global configuration values that will not change
// NOTE: Update ../defaultConfig.json if this changes
interface Config {
    socketPath: string;
    leds: {
        stringLength: number;
        regions: Record<string, [number, number]>;
    };
    interpolator: {
        //TODO properly indicate that it must be >=0
        resolution: number;
    };
    onStart: {
        colorScheme: string;
        effect: string;
        interpolator: string;
        disabledColors: Array<string>;
        effectOptions: Options;
        interpolatorOptions: Options;
        general: {
            ledPower: boolean;
            brightness: number;
            maxChannel: number;
            maxAverage: number;
            redScale: number;
            greenScale: number;
            blueScale: number;
        }
    };
    network: {
        port: number;
    }

}

// Represents a color scheme
type Color = [number, number, number]

interface ColorSchemeColor {
    id: string;
    name: string;
    //TODO properly indicate that each color must be an int from 0 to 255
    rgb: Color;
}
interface ColorScheme {
    name: string;
    colors: Array<ColorSchemeColor>;
}

//Represents options
//TODO only allow just a single string if the key is a blank string
type Option = {
    name: string;
    detail: string;
    type: "frequency" | "color" | "number" | "list" | "bool" | "button";
    value: any;
}
type FrequencyOption = Option & {
    type: "frequency";
    value: number;
}
type ColorOption = Option & {
    type: "color";
    value: Color;
}
type NumberOption = Option & {
    type: "number"
    value: number;
    min?: number;
    max?: number;
    step?: number;
}
type ListOption = Option & {
    type: "list";
    value: string;
    style?: "radio" | "dropdown";
    choices: Array<{
        id: string;
        name: string;
        detail: string;
    }>;
}
type BoolOption = Option & {
    type: "bool";
    value: boolean;
}
type ButtonOption = Option & {
    type: "button";
    value: boolean;
}

type BoolOptions = {
    [key: string]: BoolOption
}
type Options = {
    ''?: string,
    [key: string]: string | FrequencyOption | ColorOption | NumberOption | ListOption | BoolOption | ButtonOption
}

type NameDescriptionList = {
    [key: string]: {
        name: string,
        description: string
    }
}
import Effect from './effects/effect';
import Interpolator from './interpolators/interpolator';

const path = require('path');
const fs = require('fs');
const defaultDir: string = process.cwd();

function filterFunction(fileName: string, bannedName: string, extension: string): boolean {
    // Directories should be excluded
    if (fs.lstatSync(fileName).isDirectory()) return false;

    // If a banned file name is specified, exclude the item with the banned file name
    let splitFileName: Array<string> = fileName.split('.', 2);
    if (bannedName != '' && splitFileName[0] == bannedName) return false;

    // If the extension doesn't match, exclude
    if (extension != splitFileName[1]) return false;

    // If we haven't found a reason to eliminate the file, return true
    return true;
}

/**
 * Generic loader function
 *
 * As the logic for color schemes, effects, and interpolators is the same except for a few magic values, we can use one function for all of it.
 *
 * @param {string} folder - The subfolder of the code path where the things to load are
 * @param {string} bannedName - A file with this base name will be ignored
 * @param {Array<string>} extensions - A list of file extensions to consider
 * @param {(folder: string, filename: string)=>Type} load - A function that will load one file
 * @returns {Type} - A mapping of strings to loaded things
 */
function loadGeneric<Type>(folder: string, bannedName: string, extensions: string, load: (folder: string, filename: string) => Type): Record<string, Type> {
    process.chdir(path.join(defaultDir, folder));

    let files: Array<string> = fs.readdirSync('./').filter((file: string) => filterFunction(file, bannedName, extensions));

    let result: Record<string, Type> = {};
    for (let file of files) {
        result[file.split('.')[0]] = load(folder, file);
    }
    process.chdir(defaultDir);
    return result;
}

export function loadColorSchemes(): Record<string, ColorScheme> {
    return loadGeneric('colorSchemes', '', 'json', (folder: string, file: string) => JSON.parse(fs.readFileSync(file)));
}

const jsLoad = (folder: string, file: string) => require(`./${folder}/${file}`).default

export function loadEffects(): Record<string, Effect> {
    return loadGeneric('effects', 'effect', 'js', jsLoad);
}

export function loadInterpolators(): Record<string, Interpolator> {
    return loadGeneric('interpolators', 'interpolator', 'js', jsLoad);
}
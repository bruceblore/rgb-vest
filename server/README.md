# Code overview
* `./colorSchemes`: Choices of color schemes that the light effects can use
* `./effects`: Code for the light effects
* `./interpolators`: Effects choose LED states, but interpolators can generate smooth transitions between them.
* `./loader.js`: Code to loead the contents of the folders above
* `./main.js`: Code to handle communication over the network and with the LED driver process, and to call effect and interpolator code
* `./postprocess.js`: Run the interpolator and apply any global settings. Includes just brightness for now, but might include things like color calibration in the future.
* `./rgb-server.service`: A systemd service for starting main.js when the system starts, which is enabled by `../setup.sh`.

# API

## Color schemes, effects, interpolators, LED driver

The guide for how to define a color scheme is in `./colorSchemes/README.md`. Similarly, the guide for effects is in `./effects/README.md` and the guide for interpolators is in `./interpolators/README.md`. The guide for the LED driver code is in `../pixelController/README.md`.

## Main network API operations

This is how the interface is expected to control this program. An HTTP request is sent to the path `/api`, with a query string of `?op=` one of the strings in the table below to request an operation. **It is important to use `/api`**, even though there are no other paths defined, in case another path might be defined in the future. This might be a convenient way to serve a web GUI eventually. To encourage this, all request not sent to `/api` will return a a `404 Not Found` status.

You'll find that there is a pattern to help you remember them: something in plural form lists what is available, something in singular form either checks which one is chosen or chooses one, and something ending in -options either requests a list of options or sets the options. (The one query string that breaks this pattern is `options` for general options.)

If the operation needs more information, it is passed in the body of the request. Similarly, if the API is returning a value, it will be in the body of the response. The API will return with a status of `200 OK` if it is successful and is returning a value, `204 No Content` if it is successful and is not returning a value, and `400 Bad Request` if it didn't understand the request. The client is not expected to send requests that the server cannot handle, and is expected to display an error to the user if it receives any 400 or 500 series errors. The message should generally request that the user report a bug.

The Bangle.js http function does not support request bodies. As a workaround for this and other limited HTTP options, you can use `&body=` in the URL to specify what should go in the body. If both `&body=` and a request body are present, `&body=` will be used and the request body will be ignored.

| Category      | Query string   | Operation                                     |
|---------------|----------------|-----------------------------------------------|
| Color schemes | `colorSchemes` | Request a list of all color schemes available |
| Color schemes | `colorScheme`  | By default, return the ID of the active color scheme. If the query string contains `&val=`, change the color scheme to the one with the ID matching the portion after the `=`. |
| Color schemes | `colorScheme-options` | If the request body is blank, request a list of options. If the request body contains a string, parse the string as JSON and set the options to those values. For now, color scheme options will just include checkboxes to enable or disable individual colors. |
| Effects | `effects` | Request a list of all effects available |
| Effects | `effect`  | By default, return the ID of the active effect. If the query string contains `&val=`, change the effect to the one with the ID matching the portion after the `=`. |
| Effects | `effect-options` | If the request body is blank, request a list of options. If the request body contains a string, parse the string as JSON and set the options to those values. |
| Interpolators | `interpolators` | Request a list of all interpolators available |
| Interpolators | `interpolator`  | By default, return the ID of the active interpolator. If the query string contains `&val=`, change the interpolator to the one with the ID matching the portion after the `=`. |
| Interpolators | `interpolator-options` | If the request body is blank, request a list of options. If the request body contains a string, parse the string as JSON and set the options to those values. |
| General options | `options` | If the request body is blank, request a list of general options that apply regardless of color scheme, effect, and interpolator. If the request body contains a string, parse the string as JSON and set the options to those values. |

WARNING: Different color schemes, effects, and interpolators will have different options. When changing between these, the GUI should request a new list of options for that thing. Effects might also have different options depending on the color scheme. (This mainly applies to the solid color effect, which will have a different list of colors to choose from if the color scheme changes.) Therefore, the effect options should be requested again when changing the color scheme.

This shouldn't be necessary, but it might be a good idea to request *all* options whenever anything is changed. It might also be a good idea to provide a sync button to request the state information again.

## Data formats

### Lists of color schemes, effects, and interpolators

A JSON object is returned where each key is an ID, and the value is another key/value set with the following pieces of information:

| Key  | Value |
|------|-------|
| name | A short name that could be rendered in a list |
| detail | A longer description to give the user a more in-depth understanding of what this does. If there is none, it should be a blank string. |

### Listing options

A JSON object is returned where each key is an option ID, and the value is another key/value set with at least the following pieces of information:

| Key  | Value |
|------|-------|
| name | A short name that could be rendered in a list |
| detail | A longer description to give the user a more in-depth understanding of what this does. If there is none, it should be a blank string. |
| type | The type of option that this is, which affects how it should be rendered and what valid inputs there are |
| value | The current value of the option |

The following types are valid:

| Type string | Value format                    | Description |
|-------------|---------------------------------|-------------|
| frequency   | number                          | Represents a frequency of something happening in Hz. The UI may provide per-second/per-minute conversions, period/frequency conversions, a "tap to set" feature, etc. |
| color       | color hex string                | Allows the user to select a color. The UI may wish to provide a color picker in case users are not familiar with hex strings. |
| bool        | boolean                         | Represents a value which can be toggled between true or false by the user. This might be rendered with a checkbox. |
| number      | number                          | Represents a number |
| button      | boolean                         | This represents a button in the UI. The value of the setting reported by the server has no meaning. When the button is pressed, the UI should send an `options` request to the server with the button's value set to true. Any `options` requests where the button is not pressed should have it set to false. |
| list        | Any one of the valid choice IDs | A list of options of which one can and must be selected at any time. The UI may wish to present these to the user as a dropdown menu or as radio buttons. |

Depending on the type, there may be some additional keys associated with the option:

| Type   | Additional key | Value format           | Description |
|--------|----------------|------------------------|-------------|
| number | min            | number                 | This key is optional. If it is excluded, there will be no minimum. If it is included, it represents the minimum acceptable value, inclusive. |
| number | max            | number                 | This key is optional. If it is excluded, there will be no maximum. If it is included, it represents the maximum acceptable value, inclusive. |
| number | step           | number                 | This key is optional. If it is excluded, there will be no minimum. If it is included, it represents the "jump" between acceptable values. For example, if step=1, only integers will be accepted. If step=2, only every other number will be accepted. If a minimum is specified, the first acceptable number is the minimum, followed by the minimum+step, the minimum + 2\*step, the minimum + 3\*step, etc. If no minimum is specified, zero is acceptable, along with all multiples of step above or below zero. (If the maximum is not included in these values, then it will not be possible to set the maximum.) |
| list   | style          | "radio" or "dropdown"  | This key is optional. If it is included, it specifies whether the list is expected to be presented to the user as radio buttons (those circular checkboxes of which you can only select one) or as a dropdown menu. This is only a suggestion and the UI is under no obligation to follow it. If it is excluded, the UI should decide itself. |
| list   | choices        | list of key/value sets | This key is required for all options of type list. Each entry in the list represents a choice that the user could make. They are expected to be presented to the user in the order they appear in the list. Each choice must have the following keys: <ul><li>id: The ID of the choice, to be used as the value</li><li>name: A user-friendly name for the choice</li><li>detail: A more detailed description of what it does, that might be hidden behind mouseover text, a help button, etc. If there is none, it should be a blank string. </li></ul>

Additionally, there may be a key with an empty string if the UI is expected to use a specific special interface rather than a generic generated interface rendering options. The value indicates which interface this is. (This is used for the biking effect.) This should almost never be used unless there is a good reason to. If the chosen generated interface is not supported by the UI, the UI is expected to show a warning and render a generic interface as a fallback. This does have a side effect of making an empty string an invalid name for an option, but that would be a pretty bad idea anyways.

### Setting options

A JSON string is sent where each key is the ID of an option, and each value is the value that the option should be set to. If any invalid settings are sent, the server returns a `400 Bad Request`.
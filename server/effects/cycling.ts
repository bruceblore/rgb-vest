import BaseEffect from './effect'

type EffectOptions = {
    '': 'cycling',
    frontAndBack: BoolOption,
    brake: BoolOption,
    signal: ListOption,
    redSignal: BoolOption
}

export default class Effect extends BaseEffect {
    static friendlyName = 'Cycling';
    static description = 'Make cycling and walking safer at night';

    private static OFF_COLOR: Color = [0, 0, 0];            // LED off
    private static FRONT_COLOR: Color = [255, 255, 255];    // White, to emulate headlights/front running lights
    private static BACK_COLOR: Color = [64, 0, 0];          // Dim red, to emulate rear running lights
    private static BRAKE_COLOR: Color = [255, 0, 0];        // Bright red, to emulate brake lights
    private static SIGNAL_COLOR: Color = [255, 64, 0];      // Orange, to emulate turn signals
    private static TICK_TIME: number = 250;                 // Time in ms between LED toggles

    options: EffectOptions = {
        '': 'cycling',
        frontAndBack: {
            name: 'Front and back lights',
            detail: 'Front will be white, back will be a dim red',
            type: 'bool',
            value: false,
        },
        brake: {
            name: 'Brake light',
            detail: 'Back lights up bright red. Recommended to use with automation.',
            type: 'bool',
            value: false
        },
        signal: {
            name: 'Signal',
            detail: 'Turn signals and hazards',
            type: 'list',
            value: 'off',
            choices: [
                {
                    id: 'off',
                    name: 'Off',
                    detail: ''
                },
                {
                    id: 'left',
                    name: 'Left',
                    detail: ''
                },
                {
                    id: 'right',
                    name: 'Right',
                    detail: ''
                },
                {
                    id: 'hazards',
                    name: 'Hazards',
                    detail: ''
                }
            ]
        },
        redSignal: {
            name: 'Red signal',
            detail: 'Rear signals are red rather than orange. That side\'s brake light is disabled while signaling.',
            type: 'bool',
            value: false
        }
    };
    private ALL_OFF: Array<Color>;      // All LEDs off
    private currentPowerState: boolean; // When a signal is flashing, track the current state

    constructor(config: Config, colorScheme: ColorScheme, enabledColors: Array<string>, requestTick: (time: number) => void, clearTicks: () => void) {
        super(config, colorScheme, enabledColors, requestTick, clearTicks);
        this.ALL_OFF = [];
        for (let i: number = 0; i < config.leds.stringLength; i++) {
            this.ALL_OFF.push(Effect.OFF_COLOR);
        }
        this.currentPowerState = true;
    }

    setOptions(options: EffectOptions): void {
        this.options = options;
        if (this.options.signal.value == 'off') this.currentPowerState = true;
        this.clearTicks();
        this.requestTick(0);
    }

    tick(timeout: boolean, data?: any): [Array<Color>, boolean] {
        let result: Array<Color> = [];
        Object.assign(result, this.ALL_OFF);

        /**
         * Cover the specified range of result with a given color, optionally including or excluding the middle half
         *
         * @param {Array<Color>} string - The pre-existing light string to have colors changed
         * @param {Color} color - The color to use
         * @param {number} start - The index of the first LED to color, inclusive
         * @param {number} end - Last LED + 1
         * @param {boolean} outerQuarters - If true, include the outer quarters. If false, skip them.
         * @param {boolean} middleHalf - If true, include the middle half. If false, skip it.
         * @returns {Array<Colors>} - The new state of the color string
         */
        function applyColorToRegion(string: Array<Color>, color: Color, start: number, end: number, outerQuarters: boolean, middleHalf: boolean): Array<Color> {
            let length: number = end - start;
            let Q1: number = Math.round(start + (length / 4));
            let Q3: number = Math.round(start + (3 * length / 4));

            if (outerQuarters) {
                for (let i: number = start; i < Q1; i++)
                    string[i] = color;
                for (let i: number = Q3; i < end; i++)
                    string[i] = color;
            }

            if (middleHalf)
                for (let i: number = Q1; i < Q3; i++)
                    string[i] = color;

            return string;
        }

        let region: [number, number];
        if (this.options.frontAndBack.value) {
            // Potentially weird-looking code: !['left', 'hazards'].includes(this.options.signal.value)
            // Include the middle if we are not in left signal or hazards mode. Do this by checking that our current mode is not in that list.
            region = this.config.leds.regions['frontLeft'];
            result = applyColorToRegion(result, Effect.FRONT_COLOR, region[0], region[1], true,
                !['left', 'hazards'].includes(this.options.signal.value));
            region = this.config.leds.regions['frontRight'];
            result = applyColorToRegion(result, Effect.FRONT_COLOR, region[0], region[1], true,
                !['right', 'hazards'].includes(this.options.signal.value));

            // Back has an additional condition: We can either be in red mode (red getting brighter and dimmer), or the stuff from above
            // Check red mode first because it should be faster

            region = this.config.leds.regions['backLeft'];
            result = applyColorToRegion(result, Effect.BACK_COLOR, region[0], region[1], true,
                this.options.redSignal.value || !['left', 'hazards'].includes(this.options.signal.value));
            region = this.config.leds.regions['backRight'];
            result = applyColorToRegion(result, Effect.BACK_COLOR, region[0], region[1], true,
                this.options.redSignal.value || !['right', 'hazards'].includes(this.options.signal.value));
        }

        if (this.options.brake.value) {
            // For the outer quarters, draw if orange signals or we aren't signaling
            // For the inner half, draw if we aren't signaling
            region = this.config.leds.regions['backLeft'];
            result = applyColorToRegion(result, Effect.BRAKE_COLOR, region[0], region[1],
                !this.options.redSignal.value || !['left', 'hazards'].includes(this.options.signal.value),
                !['left', 'hazards'].includes(this.options.signal.value));
            region = this.config.leds.regions['backRight'];
            result = applyColorToRegion(result, Effect.BRAKE_COLOR, region[0], region[1],
                !this.options.redSignal.value || !['right', 'hazards'].includes(this.options.signal.value),
                !['rught', 'hazards'].includes(this.options.signal.value));
        }

        if (timeout) {
            this.currentPowerState = !this.currentPowerState;
        }

        if (this.options.signal.value != 'off' && this.currentPowerState) {
            // The front signals are always orange
            let color: Color = Effect.SIGNAL_COLOR;

            // Outer quarters are drawn if running is off
            // Inner half is always drawn
            if (this.options.signal.value == 'left' || this.options.signal.value == 'hazards') {
                region = this.config.leds.regions['frontLeft'];
                result = applyColorToRegion(result, color, region[0], region[1],
                    (!this.options.frontAndBack.value), true);
            }

            if (this.options.signal.value == 'right' || this.options.signal.value == 'hazards') {
                region = this.config.leds.regions['frontRight'];
                result = applyColorToRegion(result, color, region[0], region[1],
                    (!this.options.frontAndBack.value), true);
            }

            // The back signals may be red
            color = this.options.redSignal.value ? Effect.BRAKE_COLOR : Effect.SIGNAL_COLOR;

            // Outer quarters are drawn if we are in red mode OR running and brake are off
            // Inner half is always drawn
            if (this.options.signal.value == 'left' || this.options.signal.value == 'hazards') {
                region = this.config.leds.regions['backLeft'];
                result = applyColorToRegion(result, color, region[0], region[1],
                    this.options.redSignal.value || (!this.options.frontAndBack.value && !this.options.brake.value), true);
            }

            if (this.options.signal.value == 'right' || this.options.signal.value == 'hazards') {
                region = this.config.leds.regions['backRight'];
                result = applyColorToRegion(result, color, region[0], region[1],
                    this.options.redSignal.value || (!this.options.frontAndBack.value && !this.options.brake.value), true);
            }
        }

        if (timeout && this.options.signal.value != 'off') {
            this.requestTick(Effect.TICK_TIME);
        }

        return [result, true];
    }
}
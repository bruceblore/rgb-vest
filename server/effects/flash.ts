import Effect from './effect'

type EffectOptions = {
    frequency: FrequencyOption,
    colorDomain: ListOption,
    turnOff: BoolOption,
    random: BoolOption
}

class ColorChooser {
    colors: Array<string>;; // The list of color IDs to choose from
    random: boolean;        // If true, we are choosing random colors. If false, we are choosing colors in sequence
    nextIndex: number;      // If we are choosing in sequence, this is the index of the next color to return
    colorScheme: ColorScheme;   // The color scheme that is being used, to allow a proper color to be returned, as that is what we care about

    /**
     * Create a new ColorChooser object
     *
     * This object will keep track of the necessary state information and implement the color choice logic.
     *
     * @param {Array<string>} colors - An array of color IDs that will be chosen from
     * @param {boolean} random - If true, choose random colors. If false, choose colors in sequence
     * @param {ColorScheme} colorScheme - The color scheme that the colors will be looked up from
     * @returns {ColorChooser} -The ColorChooser object
     */
    constructor(colors: Array<string>, random: boolean, colorScheme: ColorScheme) {
        this.colors = colors;
        this.random = random;
        this.colorScheme = colorScheme;
        this.nextIndex = 0;
    }

    getColor(): Color {
        let colorID: string;
        if (this.random) {
            colorID = this.colors[Math.floor(Math.random() * this.colors.length)];
        } else {
            colorID = this.colors[this.nextIndex];
            this.nextIndex = (this.nextIndex + 1) % this.colors.length;
        }
        return this.colorScheme.colors.find(color => color.id == colorID).rgb
    }
}

export default class extends Effect {
    static friendlyName = 'Flash';
    static description = 'Flash colors in a color scheme';

    options: EffectOptions = {
        frequency: {
            name: 'frequency',
            detail: 'How often the colors change',
            type: 'frequency',
            value: 3
        },
        colorDomain: {
            name: 'Random color domain',
            detail: 'The size of the regions used to set the colors',
            type: 'list',
            choices: [
                {
                    id: 'full',
                    name: 'Full strip',
                    detail: ''
                },
                {
                    id: 'regions',
                    name: 'Regions',
                    detail: ''
                },
                {
                    id: 'individual',
                    name: 'Individual LED',
                    detail: ''
                },
            ],
            value: 'regions'
        },
        turnOff: {
            name: 'Turn off between flashes',
            detail: '',
            type: 'bool',
            value: true
        },
        random: {
            name: 'Randomize full strip',
            detail: 'If enabled, randomize colors. If disabled, choose colors in sequence',
            type: 'bool',
            value: true
        }
    };
    private colorChooser: ColorChooser;
    private ALL_OFF: Array<Color>;  // All LEDs off
    private currentPowerState: boolean; // When this.config.turnOff is enabled, this tracks whether the most recent update turned the LEDs on (true) or off (false)

    constructor(config: Config, colorScheme: ColorScheme, enabledColors: Array<string>, requestTick: (time: number) => void, clearTicks: () => void) {
        super(config, colorScheme, enabledColors, requestTick, clearTicks);
        this.colorChooser = new ColorChooser(
            enabledColors,
            this.options.colorDomain.value == 'full' ? this.options.random.value : true,
            colorScheme
        );
        this.ALL_OFF = [];
        for (let i: number = 0; i < config.leds.stringLength; i++) {
            this.ALL_OFF.push([0, 0, 0]);
        }
        this.currentPowerState = true;
    }

    /**
     * Change the colors that the effect uses, clear the transition queue of the interpolator, and update the state of the LEDs
     *
     * @param colorScheme: The color scheme that will be used
     * @param enabledColors: The keys of the colors of the color scheme that are actually enabled
     */
    setColors(colorScheme: ColorScheme, enabledColors: Array<string>) {
        this.colorScheme = colorScheme;
        this.enabledColors = enabledColors;
        this.colorChooser = new ColorChooser(
            enabledColors,
            this.options.colorDomain.value == 'full' ? this.options.random.value : true,
            colorScheme
        );
        this.clearTicks();
        this.requestTick(0);
    }

    setOptions(options: EffectOptions): void {
        this.options = options;
        this.colorChooser = new ColorChooser(
            this.enabledColors,
            this.options.colorDomain.value == 'full' ? this.options.random.value : true,
            this.colorScheme
        );
        this.clearTicks();
        this.requestTick(0);
    }

    /**
     * Get the next color state
     *
     * Assumes that the next color state should be a powered-on state
     *
    * @returns {Array<Color>} - The color state
    */
    private getColors(): Array<Color> {
        let result: Array<Color> = [];

        if (this.options.colorDomain.value == 'full') {
            let color: Color = this.colorChooser.getColor();;
            for (let i = 0; i < this.config.leds.stringLength; i++) {
                result.push(color);
            }
        } else if (this.options.colorDomain.value == 'regions') {
            result = Object.assign(result, this.ALL_OFF);
            for (let regionId in this.config.leds.regions) {
                let region = this.config.leds.regions[regionId];
                let color = this.colorChooser.getColor();
                for (let i = region[0]; i < region[1]; i++) {
                    result[i] = color;
                }
            }
        } else {
            for (let i = 0; i < this.config.leds.stringLength; i++) {
                result[i] = this.colorChooser.getColor();
            }
        }

        return result;
    }

    tick(timeout: boolean, data?: any): [Array<Color>, boolean] {
        let period = 1000 / this.options.frequency.value;
        let result: Array<Color> = [];

        if (this.options.turnOff.value) {
            if (this.currentPowerState) {
                result = this.ALL_OFF;
                this.currentPowerState = false;
            } else {
                result = this.getColors();
                this.currentPowerState = true;
            }
            period /= 2;    // If we are turning off between flashes, half the period to consider both the on and off time
        } else {
            result = this.getColors();
        }

        if (timeout) this.requestTick(period);
        return [result, false];

    }
}
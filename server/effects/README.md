# File loading

All files in this folder with a `.js` or `.ts` file extension, but NOT with a `.d.ts` file extension, will be attempted to be loaded as effects, except for `effect.ts`. If you do not wish to load a file, you might wish to give it another extension, like `.ts.disabled`. The part of the file before the `.js` or `.ts` will be the ID of the color scheme.

`effect.ts` is just a valid but empty effect that other effects should inherit from.

# File format

The effect files should be Javascript modules which can be `require()`ed by file name. They export a class called Effect. Detailed descriptions of what is available to be defined will be in `effect.ts`.
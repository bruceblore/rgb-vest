export default class {
    static friendlyName = 'Base effect';
    static description = 'What all effects inherit from';

    //TODO properly indicate the properties that the interpolator and requestTick must have
    //TODO properly indicate that items in enabledColors must be keys in colorScheme
    options: Options = {};
    config: Config;
    colorScheme: ColorScheme;
    enabledColors: Array<string>;
    requestTick: (time: number) => void;
    clearTicks: () => void

    /**
     * Initializes the function with the data needed to work properly
     *
     * @param config: The global configuration
     * @param colorScheme: The color scheme that will be used
     * @param enabledColors: The keys of the colors of the color scheme that are actually enabled
     * @param requestTick: A function that will be called in order to request tick() to be run in some amount of time
     * @param clearTicks: A function that will be called to clear all prior requested ticks
     */
    constructor(config: Config, colorScheme: ColorScheme, enabledColors: Array<string>, requestTick: (time: number) => void, clearTicks: () => void) {
        this.config = config;
        this.colorScheme = colorScheme;
        this.enabledColors = enabledColors;
        this.requestTick = requestTick;
        this.clearTicks = clearTicks;
    }

    /**
     * Change the colors that the effect uses, clear the transition queue of the interpolator, and update the state of the LEDs
     *
     * @param colorScheme: The color scheme that will be used
     * @param enabledColors: The keys of the colors of the color scheme that are actually enabled
     */
    setColors(colorScheme: ColorScheme, enabledColors: Array<string>) {
        this.colorScheme = colorScheme;
        this.enabledColors = enabledColors;
        this.clearTicks();
        this.requestTick(0);
    }

    /**
     * @return the current options
     */
    getOptions(): Options {
        return this.options;
    }

    /**
     * Set options and respond appropriately
     *
     * @param options: The new options that will be set
     */
    setOptions(options: Options) {
        this.options = options;
        this.clearTicks();
        this.requestTick(0);
    }

    /**
     * Update the state of the LEDs
     *
     * @param {boolean} timeout - If true, this tick is either the initial tick, or was requested by the effect. If false, something else is just requesting the current state again.
     * @param {any} data - Data that the effect requested to have passed to itself on a tick with a timeout. Useful if the effect needs to know which specific timeout this is
     * @return a 2 element array where the first is an array of colors and the second indicates whether to snap
     */
    tick(timeout: boolean, data?: any): [Array<Color>, boolean] {
        let result: Array<Color> = [];
        for (let i = 0; i < this.config.leds.stringLength; i++)
            result.push([0, 0, 0]);
        return [result, false];

    }
}
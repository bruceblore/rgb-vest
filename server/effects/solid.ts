import Effect from './effect';

// This is necessary because otherwise typescript gets mad about me accessing properties of options
// Some properties never change and therefore technically could be set here, but we'd still have to set them in the other place so there's no reason to.
interface EffectOptions extends Options {
    schemeColor: {
        name: string;
        detail: string;
        type: 'list';
        value: string;
        style?: 'radio' | 'dropdown';
        choices: Array<{
            id: string;
            name: string;
            detail: string;
        }>
    };
    useCustom: {
        name: string;
        detail: string;
        type: "bool";
        value: boolean;
    };
    custom: {
        name: string;
        detail: string;
        type: "color";
        value: Color;
    };
};

export default class extends Effect {
    static friendlyName = 'Solid color';
    static description = 'Just a solid color';

    // Typescript gets mad if we don't initialize this, even though it should actually be initialized later
    options: EffectOptions = {
        schemeColor: {
            name: '',
            detail: '',
            type: 'list',
            value: '',
            choices: []
        },
        useCustom: {
            name: '',
            detail: '',
            type: "bool",
            value: false
        },
        custom: {
            name: '',
            detail: '',
            type: 'color',
            value: [0, 0, 0]
        }
    };

    // Constructor is the same, except for calling setColors because we need to generate the options based on the colors
    constructor(config: Config, colorScheme: ColorScheme, enabledColors: Array<string>, requestTick: (time: number) => void, clearTicks: () => void) {
        super(config, colorScheme, enabledColors, requestTick, clearTicks);
        this.setColors(colorScheme, enabledColors);
    }

    // Same as super, but also update the options based on the selected color
    setColors(colorScheme: ColorScheme, enabledColors: string[]): void {
        // Note: Not calling super to do this because there is work to be done in between changing the color scheme and doing a tick.
        this.colorScheme = colorScheme;

        // This particular effect doesn't care about enabled colors. Generate our own similar list with all colors.
        let colorList = colorScheme.colors.map(color => color.id);

        // Cache the current settings, if present and still valid. Otherwise use defaults
        let schemeColor: string = colorList.includes(this.options.schemeColor.value) ?
            this.options.schemeColor.value :
            colorList[0];
        let useCustom: boolean = this.options.useCustom.value;
        let custom: Color = this.options.custom.value;

        // Set the options
        this.options = {
            schemeColor: {
                name: 'Scheme color',
                detail: '',
                type: 'list',
                value: schemeColor,
                choices: colorScheme.colors.map(color => {
                    return {
                        id: color.id,
                        name: color.name,
                        detail: `rgb (${color.rgb[0]}, ${color.rgb[1]}, ${color.rgb[2]})`
                    }
                })
            },
            useCustom: {
                name: 'Use custom?',
                detail: 'Whether the custom color should be used instead of the scheme color',
                type: 'bool',
                value: useCustom
            },
            custom: {
                name: 'Custom color',
                detail: '',
                type: 'color',
                value: custom
            }
        }

        // Tick at end
        this.tick();
    }

    tick(): [Array<Color>, boolean] {
        let color: Color = this.options.useCustom.value ?
            this.options.custom.value :
            this.colorScheme.colors.find(color => color.id == this.options.schemeColor.value).rgb;

        let result: Array<Color> = [];
        for (let i = 0; i < this.config.leds.stringLength; i++) result.push(color);
        return [result, false];
    }
}
import Effect from './effect'
const child_process = require('child_process');

type EffectOptions = {
    confirm: ButtonOption
}

class ColorChooser {
    colors: Array<string>;; // The list of color IDs to choose from
    random: boolean;        // If true, we are choosing random colors. If false, we are choosing colors in sequence
    nextIndex: number;      // If we are choosing in sequence, this is the index of the next color to return
    colorScheme: ColorScheme;   // The color scheme that is being used, to allow a proper color to be returned, as that is what we care about

    /**
     * Create a new ColorChooser object
     *
     * This object will keep track of the necessary state information and implement the color choice logic.
     *
     * @param {Array<string>} colors - An array of color IDs that will be chosen from
     * @param {boolean} random - If true, choose random colors. If false, choose colors in sequence
     * @param {ColorScheme} colorScheme - The color scheme that the colors will be looked up from
     * @returns {ColorChooser} -The ColorChooser object
     */
    constructor(colors: Array<string>, random: boolean, colorScheme: ColorScheme) {
        this.colors = colors;
        this.random = random;
        this.colorScheme = colorScheme;
        this.nextIndex = 0;
    }

    getColor(): Color {
        let colorID: string;
        if (this.random) {
            colorID = this.colors[Math.floor(Math.random() * this.colors.length)];
        } else {
            colorID = this.colors[this.nextIndex];
            this.nextIndex = (this.nextIndex + 1) % this.colors.length;
        }
        return this.colorScheme.colors.find(color => color.id == colorID).rgb
    }
}

export default class extends Effect {
    static friendlyName = 'Power off';
    static description = 'Turn off the LEDs and cleanly shut down the pi';

    options: EffectOptions = {
        confirm: {
            name: 'Turn off the pi',
            detail: '',
            type: 'button',
            value: false
        }
    };

    private colorChooser: ColorChooser;
    private ALL_OFF: Array<Color>;  // All LEDs off
    private currentPowerState: boolean; // When this.config.turnOff is enabled, this tracks whether the most recent update turned the LEDs on (true) or off (false)

    constructor(config: Config, colorScheme: ColorScheme, enabledColors: Array<string>, requestTick: (time: number) => void, clearTicks: () => void) {
        super(config, colorScheme, enabledColors, requestTick, clearTicks);
        this.ALL_OFF = [];
        for (let i: number = 0; i < config.leds.stringLength; i++) {
            this.ALL_OFF.push([0, 0, 0]);
        }
        this.currentPowerState = true;
    }

    setOptions(options: EffectOptions) {
        super.setOptions(options);
        if (options.confirm.value) {
            child_process.exec('sudo shutdown now');
        }
    }

    tick(timeout: boolean, data?: any): [Array<Color>, boolean] {
        return [this.ALL_OFF, false];
    }
}
import Effect from './effect'

type EffectOptions = {
    frequency: FrequencyOption
};

export default class extends Effect {
    static friendlyName = 'Ultra high visibility';
    static description = 'Rapid white flash to help people see you';

    options: EffectOptions = {
        frequency: {
            name: 'Frequency',
            detail: 'The frequency of the flash',
            type: 'frequency',
            value: 10
        }
    };
    private OFF: Array<Color>;
    private ON: Array<Color>;
    private currentPowerState: boolean;

    constructor(config: Config, colorScheme: ColorScheme, enabledColors: Array<string>, requestTick: (time: number) => void, clearTicks: () => void) {
        super(config, colorScheme, enabledColors, requestTick, clearTicks);
        this.OFF = [];
        this.ON = [];
        for (let i: number = 0; i < config.leds.stringLength; i++) {
            this.OFF.push([0, 0, 0]);
            this.ON.push([255, 255, 255]);
        }
        this.currentPowerState = false;
    }

    tick(timeout: boolean, data?: any): [Array<Color>, boolean] {
        let period = 500 / this.options.frequency.value;    // We are turning off between flashes, so half the period to consider both the on and off time
        let result: Array<Color> = [];

        if (timeout) this.requestTick(period);

        if (this.currentPowerState) {
            this.currentPowerState = false;
            return [this.OFF, false];
        } else {
            this.currentPowerState = true;
            return [this.ON, false];
        }
    }
}
# File loading

All files in this folder with a `.json` file type will be attempted to be loaded. If you do not wish to load a file, you might wish to give it another extension, like `.json.disabled`. The part of the file before the `.json` will be the ID of the color scheme.

# File format

At the top level, there are two keys: `name` and `colors`. The value of the `name` key is a user friendly name that is expected to be displayed in the UI. The value of the `colors` key is a list of key/value pairs, where each pair represents a color in the color scheme. The keys are `id` which represents a unique ID, `name` which represents a user friendly name that is expected to be displayed in the UI, and `rgb` is a list of 3 numbers from 0 to 255 representing the brightness of the red, green, and blue channels.
import { ClientRequest, IncomingMessage } from 'http';
import Effect from './effects/effect';
import Interpolator from './interpolators/interpolator';

const loader = require('./loader');
const fs = require('fs');
const express = require('express');
const http = require('http');
const qs = require('querystring'); // We don't really need this, we just need to use a return type for typescript

const config: Config = JSON.parse(fs.readFileSync('/etc/rgb-vest/config.json'));
const colorSchemes: Record<string, ColorScheme> = loader.loadColorSchemes();
const effects: Record<string, typeof Effect> = loader.loadEffects();
const interpolators: Record<string, typeof Interpolator> = loader.loadInterpolators();
let generalSettings = config.onStart.general;

// Communicate with the LED controller process to set the LED state
function setLEDState(ledState: Array<Color>) {
    let adjustedLedState: Array<Color> = ledState;

    if (!generalSettings.ledPower)
        // If the LEDs are turned off, just replace the input with zeros
        adjustedLedState = adjustedLedState.map(() => [0, 0, 0]);
    else {
        // Scale for brightness and color calibration
        adjustedLedState = adjustedLedState.map(color => [
            color[0] * generalSettings.brightness * generalSettings.redScale,
            color[1] * generalSettings.brightness * generalSettings.greenScale,
            color[2] * generalSettings.brightness * generalSettings.blueScale
        ]);

        // Perform extra scaling if the maximum subpixel brightness is too high
        let maxArr = function (items: Array<number>): number {
            return items.reduce((acc, item) => acc >= item ? acc : item);
        }

        let multiplyAll = function (colors: Array<Color>, multiplier: number): Array<Color> {
            let multiplyColor = function (color: Color, multiplier: number): Color {
                return [
                    color[0] * multiplier,
                    color[1] * multiplier,
                    color[2] * multiplier
                ];
            }
            return colors.map(color => multiplyColor(color, multiplier));
        }

        let maxChannel: number = maxArr(adjustedLedState.map(color => maxArr(color)));
        if (maxChannel > generalSettings.maxChannel)
            adjustedLedState = multiplyAll(adjustedLedState,
                generalSettings.maxChannel / maxChannel);

        // Perform extra scaling if the average brightness is too high
        let getAverage = function (items: Array<number>): number {
            return items.reduce((acc, item) => acc + item) / items.length;
        }
        let average: number = getAverage(adjustedLedState.map(color => getAverage(color)));
        if (average > generalSettings.maxAverage)
            adjustedLedState = multiplyAll(adjustedLedState, generalSettings.maxAverage / average);

        // Finally, round to the nearest int and clamp to [0, 255].
        adjustedLedState = adjustedLedState.map(color => [
            Math.min(255, Math.max(0, Math.round(color[0]))),
            Math.min(255, Math.max(0, Math.round(color[1]))),
            Math.min(255, Math.max(0, Math.round(color[2])))
        ]);
    }

    let timeToNextTry = 100;
    let sendRequest = () => {
        let request = http.request({
            socketPath: config.socketPath,
            path: '/colors',
            method: 'POST'
        });
        request.on('error', () => {
            console.log(`Warning: failed to connect to socket. Trying again in ${timeToNextTry / 1000} sec.`)
            setTimeout(sendRequest, timeToNextTry);
            timeToNextTry = Math.min(timeToNextTry * 1.5, 10000);
        });
        request.on('response', (response: IncomingMessage) => {
            if (response.statusCode >= 300) {
                console.log(`Warning: pixel controller returned unexpected status ${response.statusCode} ${response.statusMessage}`);
            }
        })
        request.end(JSON.stringify(adjustedLedState));
    }
    sendRequest();
}

/**
 * Perform a tick
 *
 * A "tick" is an update to the colors on the string. It involves:
 * 1. A call into the effect, to get the next colors to use and whether to snap in step 2
 * 2. A call into the interpolator, to either request a transition to the next colors or a snap
 * 3. A call by the interpolator to update the LEDs
 *
 * @param {boolean} timeout - If true, this tick was requested by the effect with a timeout other than zero. Useful if there should be different behavior in this case.
 * @param {any} data - Data that the effect requested to have passed to itself on a tick with a timeout. Useful if the effect needs to know which specific timeout this is
 */
function tick(timeout: boolean, data?: any) {
    let effectResult: [Array<Color>, boolean] = effect.tick(timeout, data);
    let colors: Array<Color> = effectResult[0];
    let snap: boolean = effectResult[1];

    if (snap) interpolator.snapToNextColors(colors);
    else interpolator.setNextColors(colors);
}

//TODO fix typing
// let timeouts: Array<ReturnType<typeof setTimeout>> = [];
let timeouts: Array<number> = [];
function requestTick(time: number, data?: any) {
    function getClosure(data?: any): Function {
        return function () {
            tick(true, data);
        }
    }

    if (time == 0) {
        tick(true, data);
    } else {
        timeouts.push(setTimeout(getClosure(data), time));
    }
}
function clearTicks() {
    for (let timeout of timeouts) {
        clearTimeout(timeout);
    }
    timeouts = [];
}

// Initialize the color scheme, enabled colors, interpolator, and effect
let colorScheme: ColorScheme = colorSchemes[config.onStart.colorScheme];
let enabledColors: Array<string> = colorScheme.colors.map((entry: ColorSchemeColor) => entry.id).filter((id: string) => !config.onStart.disabledColors.includes(id));

let initialColors: Array<Color> = [];
for (let i = 0; i < config.leds.stringLength; i++) initialColors.push([0, 0, 0]);
let interpolator = new interpolators[config.onStart.interpolator](config, setLEDState, initialColors);

let effect: Effect = new effects[config.onStart.effect](config, colorScheme, enabledColors, requestTick, clearTicks);

let colorSchemeID: string = config.onStart.colorScheme;
let effectID: string = config.onStart.effect;
let interpolatorID: string = config.onStart.interpolator;

// Functions to change the color scheme, enabled colors, interpolator, and effect. Call these rather than doing it manually to insure that everything that is dependent on them gets updated.
//NOTE: cannot use these functions for the initialization because of circular dependencies. Changing the color scheme requires an established effect, and changing the effect requires an established color scheme. Changing the interpolator requires an already initiated interpolator.
function changeColors(newColorScheme: string, newEnabledColors?: Array<string>) {
    colorSchemeID = newColorScheme;
    colorScheme = colorSchemes[newColorScheme];
    if (newEnabledColors === undefined) {
        enabledColors = [];
        for (let color of colorScheme.colors) {
            enabledColors.push(color.id);
        }
    } else {
        enabledColors = newEnabledColors;
    }
    effect.setColors(colorScheme, enabledColors);
}
function changeInterpolator(newInterpolator: string) {
    interpolatorID = newInterpolator;
    interpolator.snapToEnd();
    let currentColors: Array<Color> = interpolator.getCurrentColors();
    interpolator = new interpolators[newInterpolator](config, setLEDState, currentColors);
    tick(false);
}
function changeEffect(newEffect: string) {
    effectID = newEffect;
    clearTicks();
    effect = new effects[newEffect](config, colorScheme, enabledColors, requestTick, clearTicks);
    tick(true);
}

// Convert an array of R, G, and B values to RGB color code
function rgbToStr(rgb: Color): string {
    return `(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`;
}

// Web server code
const server = express();

/**
 * Validate an incoming set of options
 *
 * Make sure that:
 *  - Every option that was in the initial set of options is also in the new set
 *  - Every option that is in the new set was also in the original set
 *  - Every option has every property that it needs to have, and no properties change other than value
 *  - All property values are appropriate
 */
function validateOptions(oldOptions: Options, newOptions: Options) {
    // Check that the dictionaries have the same number of keys. If the lengths are different, we know for a fact that one dictionary has extra, even without checking the individual keys, and can quickly reject. If the lengths are the same, then we can just check that every key in oldOptions is also in newOptions. And because we know that the lengths are the same, we know that newOptions can't have any extras
    if (Object.keys(oldOptions).sort().length != Object.keys(newOptions).sort().length) return false;

    for (let key in oldOptions) {
        if (!newOptions.hasOwnProperty(key)) return false;  // Fail if the option doesn't exist at all
        else if (key == '' && (oldOptions[key] != newOptions[key])) return false;    // Fail if the custom interface has changed
        else if (key == '') continue;        // A blank string should have no processing done

        // Properties that should be common to all options
        else if ((oldOptions[key] as Option).name != (newOptions[key] as Option).name) return false;
        else if ((oldOptions[key] as Option).detail != (newOptions[key] as Option).detail) return false;
        else if ((oldOptions[key] as Option).type != (newOptions[key] as Option).type) return false;

        // Validation for each type of option
        switch ((oldOptions[key] as Option).type) {
            case "frequency":
                if (typeof ((newOptions[key] as FrequencyOption).value) != 'number') return false;
                else if ((newOptions[key] as FrequencyOption).value <= 0) return false;
                break;

            case "color":
                let colorOption: ColorOption = (newOptions[key] as ColorOption)
                if (
                    typeof colorOption.value != 'object' ||
                    typeof colorOption.value[0] != 'number' ||
                    typeof colorOption.value[1] != 'number' ||
                    typeof colorOption.value[2] != 'number'
                )
                    return false;
                else if (
                    (newOptions[key] as ColorOption).value[0] < 0 ||
                    (newOptions[key] as ColorOption).value[0] > 255
                )
                    return false;
                else if (
                    (newOptions[key] as ColorOption).value[1] < 0 ||
                    (newOptions[key] as ColorOption).value[1] > 255
                )
                    return false;
                else if (
                    (newOptions[key] as ColorOption).value[2] < 0 ||
                    (newOptions[key] as ColorOption).value[2] > 255
                )
                    return false;
                break;

            case "number":
                let numberOption: NumberOption = newOptions[key] as NumberOption
                if (typeof numberOption.value != 'number') return false
                else if (
                    numberOption.min != (oldOptions[key] as NumberOption).min ||
                    numberOption.max != (oldOptions[key] as NumberOption).max ||
                    numberOption.step != (oldOptions[key] as NumberOption).step
                )
                    return false;
                else if (typeof numberOption.max == 'number' && numberOption.value > numberOption.max) return false

                // If we do have a set minimum, the value must be >= the minimum, and the step is based on the minimum
                else if (typeof numberOption.min == 'number') {
                    if (numberOption.value < numberOption.min) return false;
                    else if (
                        typeof numberOption.step == 'number' &&
                        ((numberOption.value - numberOption.min) / numberOption.step) % 1 != 0  // (value - min) % step was producing inaccurate results because of floating point numbers.
                    )
                        return false;
                }

                // If we do not have a set minimum, step is based on zero
                else if (
                    typeof numberOption.step == 'number' &&
                    numberOption.value % numberOption.step != 0
                )
                    return false
                break;

            case "list":
                let newListOption: ListOption = newOptions[key] as ListOption;
                let oldListOption: ListOption = oldOptions[key] as ListOption;
                if (typeof newListOption.value != 'string') return false;
                else if (newListOption.style != (oldOptions[key] as ListOption).style) return false;

                // Make sure our value is a valid list choice
                else if (!newListOption.choices.map(choice => choice.id).includes(newListOption.value)) return false;

                // Check choices list for equality
                else if (newListOption.choices.length != oldListOption.choices.length) return false;
                for (let i = 0; i < newListOption.choices.length; i++) {
                    if (
                        newListOption.choices[i].id != oldListOption.choices[i].id ||
                        newListOption.choices[i].name != oldListOption.choices[i].name ||
                        newListOption.choices[i].detail != oldListOption.choices[i].detail
                    ) return false;
                }

                break;

            // Boolean and button can be handled together because they'd just use the same logic anyways
            case "bool":
            case "button":
                if (typeof (newOptions[key] as BoolOption).value != 'boolean') return false;
                break;

            // The type must be one of the allowed types
            default:
                return false;
        }
    }

    return true;
}

function api(request: typeof express.Request, response: typeof express.Response) {
    // Ugly useless function because Typescript doesn't like when a variable that could be a string or {} is given to something that expects a string, but the express middleware still gives us {} for an empty string
    let forceBodyToString = function (body: string | {}): string {
        if (typeof (body) === 'string') return body;
        else return '';
    }

    let query: ReturnType<typeof qs.parse> = request.query;
    let body: string = query.hasOwnProperty('body') ?
        query.body :
        forceBodyToString(request.body);

    if (query.op == 'colorSchemes') {
        let responseJSON: NameDescriptionList = {}
        for (let id in colorSchemes) {
            responseJSON[id] = {
                name: colorSchemes[id].name,
                description: colorSchemes[id].colors.map(color => color.name).join('. ')
            }
        };
        response.status(200).send(JSON.stringify(responseJSON));
    }

    else if (query.op == 'colorScheme') {
        if (query.hasOwnProperty('val')) {
            if (colorSchemes.hasOwnProperty(query.val)) {
                changeColors(query.val);
                response.status(204).send('204 No Content');
            } else {
                response.status(400).send('400 Bad Request: Unknown color scheme')
            }
        } else {
            response.send(colorSchemeID);
        }
    }

    else if (query.op == 'colorScheme-options') {
        let enabledColorsJSON: BoolOptions = {};
        for (let color of colorScheme.colors) {
            enabledColorsJSON[color.id] = {
                name: color.name,
                detail: rgbToStr(color.rgb),
                type: "bool",
                value: enabledColors.includes(color.id)
            };
        }

        if (body === '') {
            response.status(200).send(JSON.stringify(enabledColorsJSON));
        } else {
            let newColorsJSON: BoolOptions;
            try {
                newColorsJSON = JSON.parse(body);
                if (validateOptions(enabledColorsJSON, newColorsJSON)) {
                    let newEnabledColors = [];
                    for (let color in newColorsJSON)
                        if (newColorsJSON[color].value)
                            newEnabledColors.push(color);
                    changeColors(colorSchemeID, newEnabledColors);
                    response.status(204).send("204 No Content");
                } else {
                    response.status(400).send('400 Bad Request: Invalid options')
                }
            } catch {
                response.status(400).send("400 Bad Request");
            }

        }
    }

    else if (query.op == 'effects') {
        let responseJSON: NameDescriptionList = {}
        for (let id in effects) {
            responseJSON[id] = {
                name: effects[id].friendlyName,
                description: effects[id].description
            }
        };
        response.send(JSON.stringify(responseJSON));
    }

    else if (query.op == 'effect') {
        if (query.hasOwnProperty('val')) {
            if (effects.hasOwnProperty(query.val)) {
                changeEffect(query.val);
                response.status(204).send('204 No Content');
            } else {
                response.status(400).send('400 Bad Request: Unknown effect')
            }
        } else {
            response.send(effectID);
        }
    }

    else if (query.op == 'effect-options') {
        if (body === '') {
            response.status(200).send(JSON.stringify(effect.options));
        } else {
            try {
                let newOptions: Options = JSON.parse(body);
                if (validateOptions(effect.getOptions(), newOptions)) {
                    effect.setOptions(newOptions);
                    response.status(204).send('204 No Content');
                } else {
                    response.status(400).send('400 Bad Request: Invalid options');
                }
            } catch {
                response.status(400).send('400 Bad Request');
            }
        }
    }

    else if (query.op == 'interpolators') {
        let responseJSON: NameDescriptionList = {}
        for (let id in interpolators) {
            responseJSON[id] = {
                name: interpolators[id].friendlyName,
                description: interpolators[id].description
            }
        };
        response.send(JSON.stringify(responseJSON));
    }

    else if (query.op == 'interpolator') {
        if (query.hasOwnProperty('val')) {
            if (interpolators.hasOwnProperty(query.val)) {
                changeInterpolator(query.val);
                response.status(204).send('204 No Content');
            } else {
                response.status(400).send('400 Bad Request: Unknown interpolator')
            }
        } else {
            response.send(interpolatorID);
        }
    }

    else if (query.op == 'interpolator-options') {
        if (body === '') {
            response.status(200).send(JSON.stringify(interpolator.options));
        } else {
            try {
                let newOptions: Options = JSON.parse(body);
                if (validateOptions(interpolator.getOptions(), newOptions)) {
                    interpolator.setOptions(newOptions);
                    response.status(204).send('204 No Content');
                } else {
                    response.status(400).send('400 Bad Request: Invalid options');
                }
            } catch {
                response.status(400).send('400 Bad Request');
            }
        }
    }

    //TODO add validation
    //TODO explore ways to adjust numbers over a broad range non-tediously
    else if (query.op == 'options') {
        let options: Options = {
            'ledPower': {
                name: 'LED On/Off',
                detail: '',
                type: 'bool',
                value: generalSettings.ledPower
            },
            'brightness': {
                name: 'Brightness',
                detail: 'Unconditional brightness scale',
                type: 'number',
                value: generalSettings.brightness,
                min: 0,
                max: 1,
                step: 0.05
            },
            'maxChannel': {
                name: 'Max channel',
                detail: 'Maximum brightness of individual LED subpixel',
                type: 'number',
                value: generalSettings.maxChannel,
                min: 0,
                max: 255,
                step: 5
            },
            'maxAverage': {
                name: 'Max average',
                detail: 'Maximum average brightness of string',
                type: 'number',
                value: generalSettings.maxAverage,
                min: 0,
                max: 255,
                step: 5
            },
            'redScale': {
                name: 'Red scale',
                detail: '',
                type: 'number',
                value: generalSettings.redScale,
                min: 0,
                max: 1,
                step: 0.05
            },
            'greenScale': {
                name: 'Green scale',
                detail: '',
                type: 'number',
                value: generalSettings.greenScale,
                min: 0,
                max: 1,
                step: 0.05
            },
            'blueScale': {
                name: 'Blue scale',
                detail: '',
                type: 'number',
                value: generalSettings.blueScale,
                min: 0,
                max: 1,
                step: 0.05
            }
        }

        if (body === '') {
            response.status(200).send(JSON.stringify(options));
        } else {
            try {
                let newOptions: Options = JSON.parse(body);
                if (validateOptions(options, newOptions)) {
                    generalSettings.ledPower = (newOptions.ledPower as BoolOption).value;
                    generalSettings.brightness = (newOptions.brightness as NumberOption).value;
                    generalSettings.maxChannel = (newOptions.maxChannel as NumberOption).value;
                    generalSettings.maxAverage = (newOptions.maxAverage as NumberOption).value;
                    generalSettings.redScale = (newOptions.redScale as NumberOption).value;
                    generalSettings.greenScale = (newOptions.greenScale as NumberOption).value;
                    generalSettings.blueScale = (newOptions.blueScale as NumberOption).value;
                    tick(false);
                    response.status(204).send('204 No Content');
                } else {
                    response.status(400).send('400 Bad Request: Invalid options');
                }
            } catch {
                response.status(400).send('400 Bad Request');
            }
        }
    }

    else { response.status(400).send('400 Bad Request: Unknown command'); }

    response.end();
}

// Provide the api code with a text view of the data being sent
server.use(express.text());

// Handle calls to the api
server.all('/api', api);

// Handle calls to everywhere else
server.all(/.*/, (request: typeof express.Request, response: typeof express.Response) => {
    response.status(404).send('404 Not Found');
});

server.listen(config.network.port);

// Launch the effect
tick(true);
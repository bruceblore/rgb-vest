#!/usr/bin/env python

import json
import requests
import sys
from urllib.parse import quote

with open('/etc/rgb-vest/config.json') as file:
    config = json.JSONDecoder().decode(file.read())

def api(op, val=None, body=None):
    port = config["network"]["port"]
    valQuery = "" if val == None else f"&val={val}"
    bodyQuery = "" if body == None else f"&body={quote(json.JSONEncoder().encode(body))}"
    url = f'http://localhost:{port}/api?op={op}{valQuery}{bodyQuery}'
    response = requests.post(url, json=body)
    print(url, body, response.status_code)

    try:
        return response.json()
    except:
        return response.text

def setEffect(effect):
    api("effect", effect)

def setOptions(category, newOptions):
    if category in [None, ""]:
        prefix = ""
    else:
        prefix = f"{category}-"

    options = api(f"{prefix}options")
    for key in newOptions:
        options[key]["value"] = newOptions[key]
    api(f"{prefix}options", None, options)

def handleSolid(args):
    colors = {
        "red": [255, 0, 0],
        "green": [0, 255, 0],
        "blue": [0, 0, 255],
        "magenta": [255, 0, 255],
        "yellow": [255, 255, 0],
        "cyan": [0, 255, 255],
        "white": [255, 255, 255],
    }
    setEffect("solid")
    setOptions("effect", {
        "useCustom": True,
        "custom": colors[args[0]]
    })

def handleFlash(args):
    effect = api("effect")
    if effect != "flash":
        setEffect("flash")

    if len(args) >= 1:
        frequency = api("effect-options")["frequency"]["value"]
        if args[0] == "faster":
            setOptions("effect", {"frequency": frequency * 1.05})
        elif args[0] == "slower":
            setOptions("effect", {"frequency": frequency / 1.05})

def handleBrightness(args):
    brightness = api("options")["brightness"]["value"]
    if args[0] == "up":
        newBrightness = max(brightness + 0.1, 0)
    elif args[0] == "down":
        newBrightness = min(brightness - 0.1, 1)
    setOptions(None, {"brightness": newBrightness})

# Register subcommands
COMMANDS = {
    "solid": handleSolid,
    "flash": handleFlash,
    "brightness": handleBrightness
}

# Check if we're trying to invoke a subcommand. If so, invoke it with the rest of the arguments
if sys.argv[1] in COMMANDS:
    COMMANDS[sys.argv[1]](sys.argv[2:])

# If not, treat it as a generic effect change
else:
    setEffect(sys.argv[1])